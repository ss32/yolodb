# yolodb

Leverages [Ultralytics](https://docs.ultralytics.com/) Yolo v8 interface to detect things in images and store the classes in a searchable database

## Requirements

Only requires the `ultralytics` package and `tqdm`.  Install with `python3 -m pip install -r requirements.txt`

## Usage

There are two operational modes: `detect` and `search`.  The default mode is `search` which allows you to search for classes within the database.  An example database is included for demonstration purposes

```
$ python3 yolo_detect.py --help
usage: yolo_detect.py [-h] [--directory DIRECTORY] [--database DATABASE] [--detect]

options:
  -h, --help            show this help message and exit
  --directory DIRECTORY
                        Path to the directory containing images
  --database DATABASE   Optional path to override default database storage path
  --detect              Activates detect mode to build the image database

```

## Search Mode

Enter the class number or name to return images containing that class.  Currently only one class at a time is supported. 

## Detect Mode

Pass the `--detect` argument along with `--directory <IMAGE_DIRECTORY>` to run detection. By default the database will be stored in the current working directory as `image_detections.json`.  The `--database` argument overrides the default path and name.