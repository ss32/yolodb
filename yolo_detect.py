from ultralytics import YOLO
from pathlib import Path
import argparse
import os
import json
import tqdm

CLASSES = {
    0: "person",
    1: "bicycle",
    2: "car",
    3: "motorcycle",
    4: "airplane",
    5: "bus",
    6: "train",
    7: "truck",
    8: "boat",
    9: "traffic light",
    10: "fire hydrant",
    11: "stop sign",
    12: "parking meter",
    13: "bench",
    14: "bird",
    15: "cat",
    16: "dog",
    17: "horse",
    18: "sheep",
    19: "cow",
    20: "elephant",
    21: "bear",
    22: "zebra",
    23: "giraffe",
    24: "backpack",
    25: "umbrella",
    26: "handbag",
    27: "tie",
    28: "suitcase",
    29: "frisbee",
    30: "skis",
    31: "snowboard",
    32: "sports ball",
    33: "kite",
    34: "baseball bat",
    35: "baseball glove",
    36: "skateboard",
    37: "surfboard",
    38: "tennis racket",
    39: "bottle",
    40: "wine glass",
    41: "cup",
    42: "fork",
    43: "knife",
    44: "spoon",
    45: "bowl",
    46: "banana",
    47: "apple",
    48: "sandwich",
    49: "orange",
    50: "broccoli",
    51: "carrot",
    52: "hot dog",
    53: "pizza",
    54: "donut",
    55: "cake",
    56: "chair",
    57: "couch",
    58: "potted plant",
    59: "bed",
    60: "dining table",
    61: "toilet",
    62: "tv",
    63: "laptop",
    64: "mouse",
    65: "remote",
    66: "keyboard",
    67: "cell phone",
    68: "microwave",
    69: "oven",
    70: "toaster",
    71: "sink",
    72: "refrigerator",
    73: "book",
    74: "clock",
    75: "vase",
    76: "scissors",
    77: "teddy bear",
    78: "hair drier",
    79: "toothbrush",
}


def make_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--directory",
        type=str,
        help="Path to the directory containing images",
        default=".",
    )
    parser.add_argument(
        "--database",
        type=str,
        help="Optional path to override default database storage path",
        default="./image_detections.json",
    )
    parser.add_argument(
        "--detect",
        action="store_true",
        help="Activates detect mode to build the image database",
    )
    return parser.parse_args()


def enumerate_files(dir: str) -> list:
    return [file for file in Path(dir).glob("*") if file.is_file()]


def image_detection(args):
    model = YOLO("yolov8m.pt")
    database_file = Path(args.database)
    output_dict = {}

    if os.path.exists(database_file):
        print(f"!!! Found existing database file: {args.database}!!!")
        print(
            "1: Overwrite existing database\n2: Append existing database\n3: Enter new database path"
        )
        user_choice = input("Select option: ")
        while not user_choice.isdigit():
            user_choice = input("Bad input, select number")
        selection = int(user_choice)
        if selection == 1:
            pass
        if selection == 2:
            output_dict = json.load(open(database_file, "r"))
        if selection == 3:
            database_file = input("Enter new database file: ")
    database = open(database_file, "w", encoding="utf-8")
    files = enumerate_files(args.directory)
    file_exts = ["jpg", "jpeg", "png", "tiff", "bmp"]
    for f in tqdm.tqdm(files):
        if str(f) in output_dict.keys():
            print(f"Found {str(f)} in existing database, skipping")
            continue
        file_ext = str(f).lower().split(".")[-1:][0]
        if file_ext not in file_exts:
            continue
        results = model(f)
        classes = []
        for r in results:
            result_dict = json.loads(r.tojson())
            for d in result_dict:
                if d["name"] not in classes:
                    classes.append(d["name"])
        if len(classes) > 0:
            output_dict[str(f)] = classes
    json.dump(output_dict, database, indent=4)


def print_classes():
    for k, v in CLASSES.items():
        print(f"{k}: {v}")


def search(args):
    dbfile = open(args.database, "r")
    db = json.load(dbfile)
    print("Item : Class")
    print_classes()
    print("\n###########################################")
    print("Enter 'exit' to finish.")
    print("Enter 'classes' to print the classes again.")
    print("###########################################\n")
    while True:
        search_string = input("Enter number or name of class to search for: ")
        if "classes" in search_string:
            print_classes()
        if "exit" in search_string:
            print("Done!")
            exit(0)
        if search_string.isdigit():
            name = CLASSES[int(search_string)]
        else:
            name = search_string
        search_results = []
        for k, v in db.items():
            for c in v:
                if c == name:
                    search_results.append(k)
        if len(search_results) > 0:
            print(f"Found {name} in:")
            for r in search_results:
                print(r)
        else:
            print(f"Couldn't find {name} in the database")


args = make_args()
if args.detect:
    if args.directory == ".":
        print(
            "Please pass the --directory argument with a path to a directory containing images."
        )
        print("Ex: python3 yolo_detect.py --detect --directory /path/to/images")
        exit(0)
    image_detection(args)
else:
    search(args)
